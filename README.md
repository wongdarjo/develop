# README #

### What is this repository for? ###

* Personal Dev and Own Study Case

### Project Technology ###

* Node
* Gulp
* BEM
* SASS
* ReactJS
* Webpack

### How do I get set up? ###

* Install Node.js
* npm install into folder that contain package.json (your main project's folder)