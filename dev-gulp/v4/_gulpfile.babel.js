"use strict";

import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import sourcemaps from 'gulp-sourcemaps';
import pipeline from 'readable-stream';
import browserSync from 'browser-sync';


// Build Directories
// ----
const baseDir = {
	app: './app',
	assets: './app/assets',
    npm: './node_modules',
    bower: './bower_components'
};

// File Sources
// ----
const sources = {
    cssVendor: {
        src: baseDir.assets + '/stylesheets/plugins.scss',
        dest: baseDir.assets + '/css'
    },
    cssApp: {
        src: baseDir.assets + '/stylesheets/app.scss',
        dest: baseDir.assets + '/css'
    },
    jsVendor: {
	    src: baseDir.assets + '/js/*.js',
	    dest: baseDir.assets + '/js'
	}
};

/*
 * For small tasks you can export arrow functions
 */
export const clean = () => del([ 'assets' ]);

// Style Vendor
// ----
export function stylePlugin() {
    return gulp.src(sources.cssVendor.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on("error", sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(sources.cssVendor.dest))
}

// Style App
// ----
export function styleApp() {
    return gulp.src(sources.cssApp.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on("error", sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(sources.cssApp.dest))
        .pipe(browserSync.stream());
}

// Javascripts
// ----
export function scripts() {
	return gulp
		.src([
			baseDir.assets + '/js/plugins/jquery.min.js',
            baseDir.assets + '/js/plugins/popper.min.js',
            baseDir.assets + '/js/plugins/bootstrap.min.js',
            baseDir.assets + '/js/plugins/modernizr.min.js',
            baseDir.assets + '/js/plugins/jquery.matchHeight.min.js',
            baseDir.assets + '/js/plugins/wow.min.js',
            baseDir.assets + '/js/plugins/slick.min.js',
            baseDir.assets + '/js/plugins/jquery.magnific-popup.min.js'
            // asset.js + '/plugins/imagesloaded.pkgd.min.js',
            // asset.js + '/plugins/masonry.pkgd.min.js'
		], { sourcemaps: true })
        .pipe(babel({ presets: ['es2015'] }))
		.pipe(concat('plugins.js', {newLine: ';'}))
		.pipe(gulp.dest(sources.jsVendor.dest))
}

// A simple task to reload the page
export function reload() {
    browserSync.reload();
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    gulp.watch(sources.styleApp.src, styleApp);
    gulp.watch(baseDir.app + "/*.html", reload);
}

export { watch as watch };

/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
var build = gulp.parallel(styleApp, watch);

/*
 * Export a default task
 */
export default build;