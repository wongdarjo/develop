/**
 *
 * Core JS for developer
 *
 */

// var $ = jQuery.noConflict();

var GeneralFunction = (function() {

    var general = function() {
        $('a[type="static"]').on('click', function(e) {
            e.preventDefault();
        });
    };

    var matchHeight = function() {
        var container = $('.eq-height');
        if (container.length > 0) {
            container.matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
        }
    };

    var toggleSlide = function() {
        var $toggle = $('.toggle');
        if ($toggle.length > 0) {
            $toggle.each(function() {
                var element = $(this),
                    elementState = element.attr('data-state');
                if (elementState != 'open') {
                    element.find('.togglec').hide();
                } else {
                    element.find('.togglet').addClass("toggleta");
                }
                element.find('.togglet').click(function() {
                    $(this).toggleClass('toggleta').next('.togglec').slideToggle(300);
                    return true;
                });

            });
        };
    };

    var accordion = function() {
        var $accordionEl = $('.accordion');
        if ($accordionEl.length > 0) {
            $accordionEl.each(function() {
                var element = $(this),
                    elementState = element.attr('data-state'),
                    accordionActive = element.attr('data-active');
                if (!accordionActive) {
                    accordionActive = 0;
                } else {
                    accordionActive = accordionActive - 1;
                };

                element.find('.acc_content').hide();

                if (elementState != 'closed') {
                    element.find('.acctitle:eq(' + Number(accordionActive) + ')').addClass('acctitlec').next().show();
                };

                element.find('.acctitle').click(function() {
                    if ($(this).next().is(':hidden')) {
                        element.find('.acctitle').removeClass('acctitlec').next().slideUp("normal");
                        $(this).toggleClass('acctitlec').next().slideDown("normal");
                    }
                    return false;
                });

            });

        }
    };

    var fullScreen = function() {
        function setHeight() {
            windowHeight = $(window).innerHeight();
            $('.full-screen').css('min-height', windowHeight);
        };

        setHeight();

        $(window).resize(function() {
            setHeight();
        });
    };

    var linkScroll = function() {
        $("a[data-scrollto]").click(function() {
            var element = $(this),
                divScrollToAnchor = element.attr('data-scrollto'),
                divScrollSpeed = element.attr('data-speed'),
                divScrollOffset = element.attr('data-offset'),
                divScrollEasing = element.attr('data-easing'),
                divScrollHighlight = element.attr('data-highlight');

            $('html,body').stop(true).animate({
                'scrollTop': $(divScrollToAnchor).offset().top - Number(divScrollOffset)
            }, Number(divScrollSpeed));
        });
    };

    var responsiveBgImage = function() {
        var sliderContainer = $('.responsive-background-image');
        var sliderElement = sliderContainer.find('.background-image');

        function setHeight() {
            sliderElement.each(function() {
                var elementWidth = sliderElement.width();
                var dataRatioWidth = $(this).attr('data-ratio-width');
                var dataRatioHeight = $(this).attr('data-ratio-height');

                // Ratio Images
                var ratioW = dataRatioWidth,
                    ratioH = dataRatioHeight,
                    ratio = ratioW / ratioH;

                var calcHeight = elementWidth / ratio,
                    calc = calcHeight;

                $(this).height(calc);
                sliderContainer.height(calc);
            });
        };
        if (sliderContainer.length > 0) {

            setHeight();

            $(window).resize(function() {
                setHeight();
            });
        };
    };

    var parallax = function() {
        var scrollTop = window.pageYOffset

        $(window).on("scroll resize", function() {
            scrollTop = window.pageYOffset;
        });

        // Image Slider
        $(".slider-parallax .img").each(function() {
            var parallaxImage = $(this),
                parallaxOffset = parallaxImage.offset().top,
                yPos,
                coords;

            $(window).on("scroll resize", function() {
                yPos = -(parallaxOffset - scrollTop) / 2;
                coords = '50% ' + yPos + 'px';

                parallaxImage.css({
                    backgroundPosition: coords
                });
            });
        });
    };

    var masonryGrid = function() {
        if ($('.masonry-grid').length > 0) {
            var $grid = $('.masonry-grid').masonry({
                itemSelector: '.grid',
                columnWidth: '.grid',
                percentPosition: true
            });

            $grid.imagesLoaded().progress(function() {
                $grid.masonry('layout');
            });
        }
    };

    /*======================================================
    =            Init Function for All Function            =
    ======================================================*/

    var init = function() {
        general();
        matchHeight();
        toggleSlide();
        accordion();
        fullScreen();
        linkScroll();
        parallax();
        masonryGrid();
    };

    // make public function
    return {

        init: init,
        matchHeight: matchHeight,
        toggleSlide: toggleSlide,
        accordion: accordion

    } /*=====  End of Init Function for All Function  ======*/


})();

(function($) {

    // USE STRICT
    "use strict";

    GeneralFunction.init();

})(jQuery); /*=====  End of Execute  ======*/