/**
 *
 * Gulp 4 Configuration
 *
 */

"use strict";

// Dependecies
// ---
const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const fileinclude = require('gulp-file-include');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const del = require('del');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const sourcemaps = require('gulp-sourcemaps');
const pipeline = require('readable-stream').pipeline;
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const workboxBuild = require('workbox-build');

// Build Directory
// ---
const baseDir = {
    app: './app',
    dist: './dist',
    assets: './app/assets',
    npm: './node_modules',
    bower: './bower_components',
    html: './_html',
    partial: './_partial'
};

// Sources
// ---
const paths = {
    cssVendor: {
        src: baseDir.assets + '/scss/plugins.scss',
        dest: baseDir.assets + '/css'
    },
    cssApp: {
        src: baseDir.assets + '/scss/app.scss',
        dest: baseDir.assets + '/css'
    },
    css: {
        dest: baseDir.assets + '/css',
        dist: baseDir.dist + '/assets/css'
    },
    js: {
        src: baseDir.assets + '/js/*.js',
        dest: baseDir.assets + '/js',
        dist: baseDir.dist + '/assets/js'
    },
    img: {
        src: baseDir.assets + '/images/**/*',
        dest: baseDir.assets + '/images',
        dist: baseDir.dist + '/assets/images'
    },
    fonts: {
        dist: baseDir.dist + '/assets/fonts'
    },
    html: {
        src: baseDir.html,
        dest: baseDir.app,
        dist: baseDir.dist
    },
    prod: {
        src: baseDir.dist
    }
};

// BrowserSync
// ---
function sync(done) {
    browserSync.init({
        server: {
            baseDir: baseDir.app
        },
        port: 3000
    });
    done();
};

// BrowserSync Reload
// ---
function browserSyncReload(done) {
    browsersync.reload();
    done();
};

// Vendor CSS
// ---
function styleVendor() {
    return gulp
        .src(paths.cssVendor.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.cssVendor.dest));
};

// App CSS
// ---
function styleApp() {
    return gulp
        .src(paths.cssApp.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.cssApp.dest))
        .pipe(browserSync.stream());
};

// JS
// ---
function scriptVendor() {
    return gulp
        .src([
            baseDir.assets + '/js/plugins/jquery.min.js',
            baseDir.assets + '/js/plugins/popper.min.js',
            baseDir.assets + '/js/plugins/bootstrap.min.js',
            baseDir.assets + '/js/plugins/modernizr.min.js',
            baseDir.assets + '/js/plugins/jquery.matchHeight.min.js',
            baseDir.assets + '/js/plugins/slick.min.js'
            // baseDir.assets + '/js/plugins/jquery.magnific-popup.min.js'
            // asset.js + '/plugins/imagesloaded.pkgd.min.js',
            // asset.js + '/plugins/masonry.pkgd.min.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('plugins.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.js.dest));
};

function script() {
    return gulp
        .src([
            baseDir.assets + '/js/core.js'
        ])
        .pipe(browserSync.stream());
};

// HTMl Partial
// --
function htmlPartial() {
    return gulp
        .src([
            baseDir.html + '/**/*.html'
        ])
        .pipe(fileinclude({
            prefix: '@@',
            indent: true,
            basepath: '@file'
        }))
        .pipe(gulp.dest(paths.html.dest))
        .pipe(browserSync.stream());
};

// Optimize
// ---
function cssCompress() {
    var plugins = [
        cssnano({
            discardComments: {
                removeAll: true
            },
            discardDuplicates: true,
            discardEmpty: true,
            discardOverridden: true
        })
    ];

    return gulp
        .src([
            baseDir.assets + '/css/plugins.css',
            baseDir.assets + '/css/app.css'
        ])
        .pipe(postcss(plugins))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.css.dest));
};

function jsCompress() {
    return pipeline(
        gulp.src([
            paths.js.dest + '/plugins.js'
        ]),
        uglify(),
        rename({suffix: '.min'}),
        gulp.dest(paths.js.dest)
    );    
};


function images() {
    return gulp
        .src(paths.img.src)
        .pipe(newer(paths.img.dest))
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        )
        .pipe(gulp.dest(paths.img.dist));
};

// Service Worker 
// --
function serviceWorkerDev() {
    return workboxBuild.generateSW({
        globDirectory: baseDir.app,
        globPatterns: [
            '**/*.{html,js,css,jpg,png}',
        ],
        swDest: baseDir.app + '/sw.js',
        clientsClaim: true,
            skipWaiting: true
        }).then(({warnings}) => {
            // In case there are any warnings from workbox-build, log them.
            for (const warning of warnings) {
            console.warn(warning);
            }
            console.info('Service worker generation completed.');
        }).catch((error) => {
            console.warn('Service worker generation failed:', error);
        });
};

function serviceWorkerProd() {
    return workboxBuild.generateSW({
        globDirectory: baseDir.dist,
        globPatterns: [
            '**/*.{html,js,css,jpg,png}',
        ],
        swDest: baseDir.dist + '/sw.js',
        clientsClaim: true,
            skipWaiting: true
        }).then(({warnings}) => {
            // In case there are any warnings from workbox-build, log them.
            for (const warning of warnings) {
            console.warn(warning);
            }
            console.info('Service worker generation completed.');
        }).catch((error) => {
            console.warn('Service worker generation failed:', error);
        });
};

// Copy to dist
// --
function move() {

    var moveTo = baseDir.dist;
    
    return gulp
        .src([
            baseDir.app + '/*.html',
            baseDir.assets + '/css/plugins.min.css',
            baseDir.assets + '/css/app.min.css',
            baseDir.assets + '/fonts/**/*.*',
            baseDir.assets + '/images/**/*.*',
            baseDir.assets + '/js/plugins.min.js',
            baseDir.assets + '/js/core.js'
        ], {
            base: baseDir.app
        })
        .pipe(gulp.dest(moveTo));
};

// Watch files
// ---
function watchFiles() {
    gulp.watch([
        baseDir.html + '/**/*',
        baseDir.partial + '/**/*',
    ], htmlPartial, browserSyncReload);
    gulp.watch(baseDir.assets + '/scss/**/*', styleApp);
    gulp.watch(baseDir.assets + '/js/**/*', script);
    gulp.watch(baseDir.assets + '/images/**/*', images);
};


// Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
// ---
const compile = gulp.series(htmlPartial, gulp.parallel(scriptVendor, styleVendor, styleApp), images, serviceWorkerDev);
const run = gulp.parallel(htmlPartial, styleApp, serviceWorkerDev, watchFiles, sync);
const build = gulp.series(htmlPartial, gulp.parallel(scriptVendor, styleVendor, styleApp), gulp.parallel(cssCompress, jsCompress, images), move, serviceWorkerProd);

// Export task
// ---
exports.htmlPartial = htmlPartial;
exports.styleVendor = styleVendor;
exports.styleApp = styleApp;
exports.scriptVendor = scriptVendor;
exports.jsCompress = jsCompress;
exports.cssCompress = cssCompress;
exports.serviceWorkerDev = serviceWorkerDev;
exports.serviceWorkerProd = serviceWorkerProd;
exports.move = move;

// Runner
// ---
exports.compile = compile;
exports.build = build;
exports.run = run;

// Define default task that can be called by just running `gulp` from cli
// ---
exports.default = run;