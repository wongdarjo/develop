// Gulp JS

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var pump = require('pump');
var browserSync = require('browser-sync');
var fileinclude = require('gulp-file-include');
var reload = browserSync.reload;

/* ========================================================
 * Paths Variable
 * ======================================================== */

// base paths for the project
var basePaths = {
    html: './_html',
    partial: './_partial',
    root: './app',
    asset: './assets',
    npm: './node_modules',
    bower: './bower_components'
}

var asset = {
    js: basePaths.root + '/assets/js',
    css: basePaths.root + '/assets/css',
    style: basePaths.root + '/assets/stylesheets',
    less: basePaths.root + '/assets/less',
    scss: basePaths.root + '/assets/scss',
    sass: basePaths.root + '/assets/sass',
    img: basePaths.root + '/assets/images',
    font: basePaths.root + '/assets/fonts'
}

var supported = [
    'last 2 versions',
    'safari >= 8',
    'ie >= 10',
    'ff >= 20',
    'ios 6',
    'android 4'
];

/* ========================================================
 * Tasks with Browser Sync
 * ======================================================== */
gulp.task('browserSync', function() {

    var files = [
        basePaths.root + '*.html',
        asset.style + '*.scss',
        asset.js + '*.js'
    ];

    browserSync.init(files, {
        server: {
            baseDir: "./app"
        },
        notify: 'true'
    });
});

/* ========================================================
 * General Tasks
 * ======================================================== */

gulp.task('html', function() {
    return gulp.src(basePaths.root + '/**/*.html')
        .pipe(reload({ stream: true }))
});

gulp.task('fileinclude', function() {
    gulp.src(['./_html/**/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            indent: true,
            basepath: '@file'
        }))
        .pipe(gulp.dest(basePaths.root))
        .pipe(reload({ stream: true }))
});

gulp.task('style', function() {
    return gulp.src(asset.style + '/app.scss')
        .pipe(plugins.plumber({
            errorHandler: function(err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass())
        .pipe(plugins.sass({
            sourceComments: 'map',
            sourceMap: 'sass',
            outputStyle: 'expanded'
        }).on('error', plugins.notify.onError(function(error) {
            return '\nProblem file : ' + error.message;
        })))
        .pipe(plugins.rename('core.css'))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(asset.css))
        .pipe(reload({ stream: true }))
});

gulp.task('style-plugins', function() {
    return gulp.src(asset.style + '/plugins.scss')
        .pipe(plugins.plumber({
            errorHandler: function(err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass())
        .pipe(plugins.sass({
            outputStyle: 'expanded'
        }).on('error', plugins.notify.onError(function(error) {
            return '\nProblem file : ' + error.message;
        })))
        .pipe(plugins.rename('plugins.css'))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(asset.css))
        .pipe(reload({ stream: true }))
});

gulp.task('js', function() {
    return gulp.src([
            asset.js + '/plugins/jquery.min.js',
            asset.js + '/plugins/bootstrap-bundle.min.js',
            asset.js + '/plugins/modernizr.min.js',
            asset.js + '/plugins/jquery.matchHeight.min.js',
            asset.js + '/plugins/wow.min.js',
            asset.js + '/plugins/slick.min.js',
            asset.js + '/plugins/jquery.magnific-popup.min.js',
            asset.js + '/plugins/imagesloaded.pkgd.min.js',
            asset.js + '/plugins/masonry.pkgd.min.js'
        ])
        .pipe(plugins.concat('plugins.js'))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(asset.js))
        .pipe(reload({ stream: true }))
});

/* ========================================================
 * Default Tasks
 * ======================================================== */

gulp.task('default', ['style', 'js', 'fileinclude', 'browserSync'], function() {
    gulp.watch('**/*.scss', { cwd: asset.style }, ['style']);
    gulp.watch('**/*.js', { cwd: asset.js }, ['js']);
    gulp.watch('**/*.html', { cwd: basePaths.partial }, ['fileinclude']);
    gulp.watch('**/*.html', { cwd: basePaths.html }, ['fileinclude']);
    // gulp.watch('**/*.html', { cwd: basePaths.root }, ['html']);
});

/* ========================================================
 * Production Tasks
 * ======================================================== */

gulp.task('optimize', function() {
    /*=================================
    =            copy file            =
    =================================*/
    // gulp.src('./*.shtml')
    //     .pipe(gulp.dest('dist/'));

    // gulp.src('inc/**/*')
    //     .pipe(gulp.dest('dist/inc'));

    // gulp.src('app/assets/fonts/*')
    //     .pipe(gulp.dest('dist/assets/fonts'));

    // gulp.src('app/assets/image/*')
    //     .pipe(gulp.dest('dist/assets/image'));


    /*==================================
    =            minify CSS            =
    ==================================*/
    gulp.src('app/assets/css/core.css')
        .pipe(gulp.dest(asset.css))
        .pipe(plugins.cssnano({
            autoprefixer: {
                browsers: supported,
                add: true
            },
            discardComments: {
                removeAll: true
            }
        }))
        .pipe(plugins.rename('core.min.css'))
        .pipe(gulp.dest(asset.css));

    gulp.src('app/assets/css/plugins.css')
        .pipe(gulp.dest(asset.css))
        .pipe(plugins.cssnano({
            autoprefixer: {
                browsers: supported,
                add: true
            },
            discardComments: {
                removeAll: true
            }
        }))
        .pipe(plugins.rename('plugins.min.css'))
        .pipe(gulp.dest(asset.css));

    /*=================================
    =            minify JS            =
    =================================*/
    gulp.src('app/assets/js/plugins.js')
        .pipe(gulp.dest('/assets/js'))
        .pipe(plugins.uglify())
        .pipe(plugins.rename('plugins.min.js'))
        .pipe(gulp.dest(asset.js));

    /*----------  move sourcemap into dist folder  ----------*/
    // gulp.src('app/assets/stylesheets/*.map')
    //     .pipe(gulp.dest('/assets/stylesheets'));

    // gulp.src('app/assets/js/*.map')
    //     .pipe(gulp.dest('/assets/js'));

});
