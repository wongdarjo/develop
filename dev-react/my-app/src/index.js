// Import the React and ReactDom library
import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import Card from './Card.js';
import Timer from './Timer.js';

function paraGraph() {
	return 'lorem ipsum dolor sit amet...'
};

// Create a react component
class App extends React.Component {	
	render() {
		const headingAlt = {
			first: 'Heading One',
			second: 'Heading Two',
			third: 'Heading Three',
			fourth: 'Heading Four'
		}		
		return (
			<main className='main-wrapper'>
				<div className='container'>
					<div className='row'>
						<div className='col-lg-12'>
							<h1 style={{color: 'red'}}>{headingAlt.first}</h1>
							<h2 style={{color: 'green'}}>{headingAlt.second}</h2>
							<h3 style={{color: 'blue'}}>{headingAlt.third}</h3>
							<h4 style={{color: 'purple'}}>{headingAlt.fourth}</h4>
							<p>{paraGraph()}</p>
						</div>
					</div>
					<div className='row'>
						<div className="col-lg-4">
							<Card
								avatar= {faker.image.avatar()}
								author= 'Sam'
								date= '12/12/2018'
								content= 'Article One'
							/>
						</div>
						<div className="col-lg-4">
							<Card 
								avatar= {faker.image.avatar()}
								author= 'Alex'
								date= '13/12/2018'
								content= 'Article Two'
							/>
						</div>
						<div className="col-lg-4">
							<Card 
								avatar= {faker.image.avatar()}
								author= 'Jane'
								date= '16/12/2018'
								content= 'Article Three'
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-12">
							<h1>Timer Countdown</h1>
							<Timer time=""/>
						</div>
					</div>
				</div>
			</main>
		);
	};
};

// Take the react component and show it on the screen
ReactDOM.render(
	<App />,
	document.querySelector('#root')
);