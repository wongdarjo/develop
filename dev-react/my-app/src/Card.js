import React from 'react';
import CardList from './CardList.js';

class Card extends React.Component {
    render() {
        return (
            <CardList>
                <img className='card-img-top' src={this.props.avatar} alt='avatar' />
                <div className='card-body'>
                    <p className='card-text'>{this.props.content}</p>
                    <p>{this.props.author}</p>
                    <p>{this.props.date}</p>
                </div>
            </CardList>
        );
    };
};

export default Card;