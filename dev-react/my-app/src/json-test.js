var btn = document.getElementById('btn'),
    content = document.getElementById('content'),
    pageCounter = 1;

btn.addEventListener('click', function() {
    var req = new XMLHttpRequest();

    req.open('GET', 'https://learnwebcode.github.io/json-example/animals-' + pageCounter + '.json');

    req.onload = function() {

        if (req.status >= 200 && req.status < 400) {

        }

        var ourData = JSON.parse(req.responseText);
        renderHTML(ourData);
    };

    req.onerror = function() {
        console.log('connection errow');
    }

    // SENT REQUEST
    req.send();

    pageCounter++;

    if (pageCounter > 3) {
        btn.style.display = 'none';
    }
});

function renderHTML(data) {
    var htmlString = "";

    for(i = 0; i < data.length; i++) {
        htmlString += "<p>" + data[i].name + " is a " + data[i].species;

        for (j = 0; j < data[i].foods.likes.length; j++) {
            htmlString += data[i].foods.likes[j];
        }

        htmlString +=  "</p>";
    }

    content.insertAdjacentHTML('beforeend', htmlString);
}