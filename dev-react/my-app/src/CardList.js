import React from 'react';

class CardList extends React.Component {    
    render() {
        // console.log(this.props.children)
        return  <div className='card'>{this.props.children}</div>
    };
};

export default CardList;