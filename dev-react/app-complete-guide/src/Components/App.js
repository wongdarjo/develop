import React from 'react';
import '../Styles/App.css';
import Person from './Person';

class App extends React.Component {

    state = {
         persons: [
            { name: 'John', age: 25 },
            { name: 'Frank Lampard', age: 34 },
            { name: 'Bima Indra', age: 24 }
         ],
         otherState: 'Another state'
    }

    hanldeSwitchName = () => {
        // console.log('Clicked');
        this.setState({
            persons: [
                { name: 'Petr', age: 25 },
                { name: 'Frank', age: 23 },
                { name: 'Taufik', age: 33 }
            ]
        })
    }

    render() {
        return (
            <div className="App">
                <h1>Hello</h1>
                <button onClick={this.hanldeSwitchName}>Switch</button>
                <Person name={this.state.persons[0].name} age={this.state.persons[0].age} />
                <Person name={this.state.persons[1].name} age={this.state.persons[1].age} />
                <Person name={this.state.persons[2].name} age={this.state.persons[2].age} />
            </div>
        )
    }
}

export default App;