import React from 'react';

// Logic
const getSeason = (lat, month) => {
    if (month > 8 && month < 12) {
        return lat > 0 ? 'Summer' : 'Winter';
    } else {
        return lat > 0 ? 'Winter' : 'Summer';
    };
};

class SeasonDisplay extends React.Component {
    render() {
        const season = getSeason(this.props.lat, new Date().getMonth());
        return (
            <div>
                <h1>Season Display</h1>
                <p>This time is {season} in Indonesia</p>
            </div>
        );
    };
};

export default SeasonDisplay;