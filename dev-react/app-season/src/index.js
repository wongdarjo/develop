import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './seasonDisplay.js';
import Spinner from './spinner.js';

class App extends React.Component {
    
    state = {
        lat: null,
        long: null,
        errorMsg: ''
    }

    // COMPONENT LIFE CYCLE
    componentDidMount() {
        // Content visible on the screen...
        
        // Call geolocation service
        window.navigator.geolocation.getCurrentPosition(
            position => {
                // we call set state !!!
                this.setState({
                    lat: position.coords.latitude, 
                    long: position.coords.longitude
                });
            },
            err => {
                console.log(err.message);
                this.setState({
                    errorMsg: err.message
                });
            }
        );
    }
    componentDidUpdate() {
        // Sit and wait for updates...
    }
    componentWillUnmount() {
        // Sit and wait until this component is not longer shown...
    }

    // Helper for render content has lots conditions
    renderContent() {
        // Conditional
        if (this.state.errorMsg && !this.state.lat && !this.state.long) {
            return <h1>Error: {this.setState.errorMsg}</h1>
        } else if (!this.state.errorMsg && this.state.lat && this.state.long) {
             return (
                 <SeasonDisplay 
                    lat={this.state.lat} 
                    long={this.state.long} 
                 />
             );
        } else {
            return <Spinner />
        };
    }

    // RENDER it !!!
    render() {
        return (
            <div className="main-wrapper">
                {this.renderContent()}
            </div>
        );
    };
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
)