import React from 'react';
import './css/spinner.css';

class Spinner extends React.Component {
    render() {
        return (
            <div className="loader">
                <span></span>
                <span></span>
                <span></span>
                <h1>{this.props.message}</h1>
            </div>
        );
    }
}

Spinner.defaultProps = {
    message: 'Loading...'
}

export default Spinner;