import React from 'react';

class SearchBar extends React.Component {

    state = {
        term: ''
    };
    
    // arrow function to handle undefined error
    handleFormSubmit = event => {
        event.preventDefault();

        this.props.onSubmit(this.state.term);
    };

    render() {
        return (
            <div>
                <form onSubmit={this.handleFormSubmit} className="mt-4 mb-4">
                    <label className="control-label">Image Search</label>
                    <input 
                        type="text"
                        className="form-control"
                        value={this.state.term}
                        onChange={(event) => this.setState({term: event.target.value})}
                        placeholder="Type here..."
                    />
                </form>
            </div>
        );
    };
};

export default SearchBar;