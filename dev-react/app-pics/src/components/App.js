import React from 'react';
import unsplash from '../api/Unsplash';
import SearchBar from './SearchBar';
import ImagesList from './ImagesList';

class App extends React.Component {

	state = {
		images: []
	}

	handleSearch = async (term) => {
		const response = await unsplash.get('/search/photos', {
			params: { query: term }			
		})
		this.setState({ images: response.data.results });
		console.log(this.state.images);
	}

    render() {
        return (
            <main className="main-wrapper">
                <div className="container">
                    <SearchBar onSubmit={this.handleSearch} />
                    <p>There are : {this.state.images.length} images.</p>
                    <ImagesList images={this.state.images} />
                </div>
            </main>
        );
    };
}

export default App;