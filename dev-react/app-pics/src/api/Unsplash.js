
import axios from 'axios';

export default axios.create({
	baseURL: 'https://api.unsplash.com',
	headers: {
		Authorization: 'Client-ID e9b87b71f68aba4bfe2d056634124535201f22f2154c61e118ef4d4bd0d2f967'
	}
});